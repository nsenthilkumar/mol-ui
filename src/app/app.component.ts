import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

import { LoginComponent } from './login/login.component';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  @ViewChild(LoginComponent) routePath;

  constructor(private toastrService: ToastrService,
              private appService: AppService) {
    this.toastrService.overlayContainer = this.toastContainer;
  }

  private toasterIns: any;
  toastTimeOut: any = 3000;

  public showErrorMsg(msg) {
    this.toasterIns = this.toastrService.error(msg, '', {
      timeOut: this.toastTimeOut,
    });
  }

  public showSuccessMsg(msg) {
    this.toasterIns = this.toastrService.success(msg, '', {
      timeOut: this.toastTimeOut,
    });
  }

  public showInfoMsg(msg) {
    this.toasterIns = this.toastrService.info(msg, '', {
      timeOut: this.toastTimeOut,
    });
  }

  public closeToaster() {
    this.toastrService.remove(this.toasterIns);
    this.toastrService.clear(this.toasterIns);
  }

  private eventListener() {
    this.appService.showToasterErr.subscribe((msg) => {
      this.showErrorMsg(msg);
    });
    this.appService.showToasterSuccess.subscribe((msg) => {
      this.showSuccessMsg(msg);
    });
    this.appService.showToasterInfo.subscribe((msg) => {
      this.showInfoMsg(msg);
    });
    this.appService.closeToasterPop.subscribe((msg) => {
      this.closeToaster();
    });
  }

  ngOnInit() {
    this.eventListener();
  }
}
