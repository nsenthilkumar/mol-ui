import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ScrollbarModule } from 'ngx-scrollbar';
import { ModalModule } from 'ngx-modialog';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TimeAgoPipe } from 'time-ago-pipe';
import { FileUploadModule } from 'ng2-file-upload';

import { MaterialModule } from './shared/modules/material.module';
import { AppRoutesModule } from './app.routing';
import { AppService } from './app.service';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/layout/header.component';
import { LoginModule } from './login/login.module';
import { LoginService } from './login/login.service';
import { AuthGuardService } from './shared/services/auth.guard.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ChartsModule,
    MaterialModule,
    ScrollbarModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot({ closeButton: true }),
    ToastContainerModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    FileUploadModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutesModule,
    LoginModule
  ],
  providers: [
    AppService,
    LoginService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
