import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { HomeModule } from './home/home.module';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './shared/services/auth.guard.service';
import { appConst } from './app.const';

const appRoutes: Routes = [
    { path: '', redirectTo: appConst.ROUTES.LOGIN, pathMatch: 'full' },
    { path: appConst.ROUTES.LOGIN, component: LoginComponent },
    { path: appConst.ROUTES.HOME, loadChildren: () => HomeModule, canActivate: [AuthGuard] },
    { path: '**', redirectTo: appConst.ROUTES.LOGIN }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
    exports: [RouterModule]
})

export class AppRoutesModule { }
