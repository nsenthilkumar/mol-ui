export const appConst = {
    LAN: 'en',
    ROUTES: {
        LOGIN: 'login',
        HOME: 'home',
        DASHBOARD: 'dashboard'
    },

    DATE_FORMAT: {
        API: 'YYYY-MM-DDThh:mm:ss',
        DATE_TIME: 'YYYY-MM-DD hh:mm:ss A',
        DATE: 'YYYY-MM-DD'
    },

    ERROR_MSG: {
        BAD_REQ: 'Bad Request',
        INTERVAL_SERVER_ERROR: 'Internal Server Error',
        ENTITY_NOT_FOUND: 'Error Code:452 ENTITY NOT FOUND IN DATABASE',
        INVALID_CREDENTIALS: 'Error Code:453 INVALID CREDENTIALS : INCORRECT USERID OR PASSWORD',
        BAD_CREDENTIALS: 'Bad credentials'
    },

    STATUS: {
        OPEN: 'Open',
        REVIEWED: 'Reviewed',
        APPROVED: 'Approved'
    }
};
