import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { appConfig } from '../../app.config';
import { AppService } from '../../app.service';
import { appConst } from '../../app.const';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    constructor(private spinnerService: Ng4LoadingSpinnerService,
                private appService: AppService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let request = req;
        const requestUrl = req.url;
        this.appService.setBusy();
        if (!requestUrl.includes('.json')) {
            const token = this.appService.userData.access_token;
            if (!requestUrl.includes('/login') && !requestUrl.includes('/logo') && !requestUrl.includes('/forgot-password')) {
                request = req.clone({
                    url: appConfig.API_BASE_URL + req.url,
                    setHeaders: {
                        'Authorization': 'Bearer ' + token,
                    }
                });
            } else {
                if (requestUrl.includes('/logo')) {
                    request = req.clone({
                        url: appConfig.API_BASE_URL + req.url,
                        setHeaders: {
                            'Authorization': 'Bearer ' + token,
                        },
                        responseType: 'text',
                    });
                } else {
                    request = req.clone({
                        url: appConfig.API_BASE_URL + req.url,
                        setHeaders: {}
                    });
                }
            }

        }
        return next
            .handle(request)
            .map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.appService.resetBusy();
                }
                return event;
            })
            .catch((err: any) => {
                this.appService.resetBusy();
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 0) {
                        this.appService.showToasterErrMsg('Server Unreachable');
                    } else {
                        if (err.status >= 500) {
                            this.appService.showToasterErrMsg(appConst.ERROR_MSG.INTERVAL_SERVER_ERROR);
                        } else {
                            if ((err.error)
                                && (err.error !== appConst.ERROR_MSG.INVALID_CREDENTIALS)
                                && (err.error !== appConst.ERROR_MSG.ENTITY_NOT_FOUND)
                                && (!err.error.error_description)) {
                                let errMsg = appConst.ERROR_MSG[err.error];
                                if (!errMsg) {
                                    errMsg = appConst.ERROR_MSG.BAD_REQ;
                                }
                                this.appService.showToasterErrMsg(errMsg);
                            }
                        }
                    }
                    return Observable.throw(err);
                }
            });
    }
}
