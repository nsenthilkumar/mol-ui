import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppHttpInterceptor } from './interceptors/app.http.interceptor';
import { NumberOnlyDirective } from "./directives/number-only.directive";
import { KeysPipe } from "./pipes/keys.pipe";

@NgModule({
  declarations: [
    NumberOnlyDirective,
    KeysPipe
  ],
  imports: [
    TranslateModule,
    NgSelectModule
  ],
  exports: [
    MaterialModule,
    TranslateModule,
    NgSelectModule,
    NumberOnlyDirective,
    KeysPipe
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
  ]
})

export class SharedModule { }
