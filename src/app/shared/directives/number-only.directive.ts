import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[num-only]'
})

export class NumberOnlyDirective {

    constructor(private elementRef: ElementRef) { }

    private regex: RegExp = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
    private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-'];

    @HostListener('keydown', ['$event'])
    public onKeyDown(event: KeyboardEvent) {
        if (this.specialKeys.indexOf(event.key) !== -1) {
            return;
        }
        const currentValue: string = this.elementRef.nativeElement.value;
        const enteredValue: string = currentValue.concat(event.key);
        if (enteredValue && !String(enteredValue).match(this.regex)) {
            event.preventDefault();
        }
    }
}