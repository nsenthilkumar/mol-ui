import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from './login.service';
import { AppService } from '../app.service';
import { appConst } from '../app.const';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {

    protected user: FormGroup;
    protected formSubmitted = false;
    private isValidUser: boolean;
    protected routePath: any;

    constructor(private router: Router,
                private form: FormBuilder,
                private appService: AppService,
                private route: ActivatedRoute,
                private loginService: LoginService,
                private translate: TranslateService) {
        this.initUserForm();
    }

    public initUserForm() {
        this.user = this.form.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.isValidUser = true;
    }

    public login() {
        this.routePath = this.route.snapshot.routeConfig.path;
        const data = this.user.value;
        this.loginService.authenticate(data).subscribe(
            res => {
                this.successLogin(res);
            },
            err => {
                this.failureLogin(err);
            }
        );
    }

    private successLogin(res) {
        this.isValidUser = true;
        this.appService.createLoginSession(res);
        this.router.navigateByUrl('/home/dashboard');
    }

    private failureLogin(err) {
        this.isValidUser = false;
    }

    ngOnInit() {
        sessionStorage.clear();
    }
}
