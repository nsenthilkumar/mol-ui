import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  public authenticate(user) {
    let fd = new FormData();
    fd.append('username', user.username);
    fd.append('password', user.password);
    // return this.http.post('/auth/login', fd);
    return this.http.get('./assets/mocks/login-res.mock.json');
  }
}
