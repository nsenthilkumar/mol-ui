import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScrollbarModule } from 'ngx-scrollbar';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoginService } from './login.service';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ScrollbarModule,
        RouterModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        LoginComponent
    ],
    providers: [
        LoginService
    ],
})
export class LoginModule { }
