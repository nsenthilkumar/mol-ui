import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ScrollbarModule } from 'ngx-scrollbar';
import { TagInputModule } from 'ngx-chips';

import { HomeRoutingModule } from './home.routing';
import { HomeComponent } from './home.component';
import { MenuComponent } from '../shared/layout/menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeService } from './home.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TagInputModule,
        NgxDatatableModule,
        ScrollbarModule,
        HomeRoutingModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        HomeComponent,
        MenuComponent,
        DashboardComponent
    ],
    providers: [
        HomeService
    ],
})
export class HomeModule { }
