import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { appConst } from '../app.const';

const homeRoutes: Routes = [
    {
        path: '', component: HomeComponent,
        children: [
            { path: appConst.ROUTES.DASHBOARD, component: DashboardComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule],
})
export class HomeRoutingModule { }
