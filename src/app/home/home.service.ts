import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppService } from '../app.service';

@Injectable()
export class HomeService {

    constructor(private http: HttpClient,
                private appService: AppService) { }

}
