import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Injectable()
export class AppService {

    constructor(private http: HttpClient,
                private spinnerService: Ng4LoadingSpinnerService) { }

    public showToasterErr: EventEmitter<any> = new EventEmitter<any>();
    public showToasterInfo: EventEmitter<any> = new EventEmitter<any>();
    public showToasterSuccess: EventEmitter<any> = new EventEmitter<any>();
    public closeToasterPop: EventEmitter<any> = new EventEmitter<any>();
    public userLoggedIn: EventEmitter<any> = new EventEmitter<any>();
    public userLoggedOut: EventEmitter<any> = new EventEmitter<any>();
    public userData: any = {};
    public selectedLanguage: any = 'en';

    public showToasterErrMsg(msg) {
        this.showToasterErr.emit(msg);
    }

    public showToasterSuccessMsg(msg) {
        this.showToasterSuccess.emit(msg);
    }

    public showToasterInfoMsg(msg) {
        this.showToasterInfo.emit(msg);
    }

    public closeToaster(isUserInvalid) {
        this.closeToasterPop.emit(isUserInvalid);
    }

    public getUserProfilePic(filePath) {
        return null;
    }

    public createLoginSession(userData) {
        this.userData = userData;
        sessionStorage.setItem('user-data', JSON.stringify(userData));
        this.userLoggedIn.emit();
    }

    public getUserData() {
        return JSON.parse(sessionStorage.getItem('user-data'));
    }

    public isUserAuthenticated() {
        if (this.userData && this.userData.access_token) {
            return true;
        }
        return false;
    }

    public updateUserData(userData) {
        this.userData.firstName = userData.firstName;
        this.userData.lastName = userData.lastName;
        this.userData.middleName = userData.middleName;
        this.userData.contactNumber = userData.contactNumber;
        sessionStorage.setItem('user-data', JSON.stringify(this.userData));
    }

    public logoutUser(data) {
        this.http.post('/auth/logout', data).subscribe(
            res => {
                this.clearSession();
            },
            err => {
                this.clearSession();
            }
        );
    }

    private clearSession() {
        this.userData = null;
        sessionStorage.clear();
        this.userLoggedOut.emit();
    }

    public setLanguage(lang) {
        this.selectedLanguage = lang;
    }

    public setBusy() {
        this.spinnerService.show();
    }

    public resetBusy() {
        this.spinnerService.hide();
    }
}
